#!/bin/bash

# Script to install a vanilla Ubuntu 18.04 lts AMD64 with strongswan vpn client
# Only 'confgurate below' part should be altered to prepare the configuration.
# Created 9th of March 2020 by Gino Eising

# script should be run a root as default for cloud init scripts

###### Configure below - begin ########
LEFTSUBNET=192.168.26.64/26 #set this manually according to VPN_SERVERS information
PSK=Z5-bogus-S5 #set this manually according to VPN_SERVERS information

###### Configure below - end ########

WANIP=$(dig +short myip.opendns.com @resolver1.opendns.com)
DEFAULTINTERFACE=$(ip -o -4 route show to default | awk '{print $5}' | uniq)
IP=$(ip addr show | awk '$1 == "inet" && $3 == "brd" { sub (/\/.*/,""); print $2 }' | head -n 1)
GATEWAY=$(echo $IP | sed 's/\.[0-9]*$/.1/')

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Prevent dpkg requesting user input
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections

apt update && DEBIAN_FRONTEND=noninteractiveapt apt install strongswan iptables-persistent -y

# Creating the routing part between ens3 and sub interface ens3:0
cat >> /etc/sysctl.conf << EOF
net.ipv4.ip_forward = 1
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
EOF

# Activate new sysctl rules
sysctl -p /etc/sysctl.conf

# Creates desired network interfaces ens3 and ens3:0
cat << EOF > /etc/netplan/50-cloud-init.yaml
network:
    version: 2
    ethernets:
      $DEFAULTINTERFACE:
        addresses:
          - $IP/24
          - $LEFTSUBNET
        gateway4: $GATEWAY
        nameservers:
          search: [mydomain, otherdomain]
          addresses: [8.8.8.8, 1.1.1.1]
EOF

netplan apply

#route add -net 193.177.238.0/24 dev ens3:0
#route add -net 193.177.238.0/24 dev virbr0
#route add -net 193.177.238.0/24 gw 192.168.122.30

# Sets iptables to create MASQUERADE between IPsec subnets
iptables -t nat -A POSTROUTING -s 193.177.238.0/24 -d $LEFTSUBNET -j MASQUERADE
# Saves created rule into a file so it works after reboot
iptables-save > /etc/iptables/rules.v4

# Creates ipsec configuration for vpn client to tennet
cat << EOF > /etc/ipsec.conf
config setup
#       strictcrlpolicy=yes
        uniqueids = yes

# Add connections here.
conn tennet
        authby=secret
        auto=route
        type=tunnel
        esp=aes256-sha256-modp2048
        ike=aes256-sha256-modp2048
        ikelifetime=86400s

        right=193.177.237.14
        rightsubnet=193.177.238.0/24
        rightid=193.177.237.14
        rightfirewall=yes

        left=$IP
        leftsubnet=$LEFTSUBNET
        leftid=$WANIP

        dpdaction=restart
        dpddelay=10
        dpdtimeout=30
        compress=no
        keyexchange=ikev2
EOF

# Creates a ipsec secret based on Tennet
cat << EOF > /etc/ipsec.secrets
# This file holds shared secrets or RSA private keys for authentication.

# RSA private key for this host, authenticating it to any other host
# which knows the public part.

# source      destination
$WANIP 193.177.237.14 : PSK "$PSK"
EOF

cat << EOF > /etc/apt/apt.conf.d/20auto-upgrades
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::Download-Upgradeable-Packages "1";
EOF

# starting IPsec
#ipsec restart
